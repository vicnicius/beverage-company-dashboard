export * from './Failed'
export * from './Loading'

export enum DataStatus {
  INITIAL = 'INITIAL',
  LOADING = 'LOADING',
  LOADED = 'LOADED',
  FAILED = 'FAILED',
}
