import { Button, Dimmer, DimmerDimmable } from 'semantic-ui-react'
import React from 'react'

export const Failed = ({ onRetryClick }: { onRetryClick: () => void }): JSX.Element => {
  return (
    <DimmerDimmable as={'div'} blurring={true} className={'Card-failed'}>
      <Dimmer active={true}>
        <p data-testid={'error-message'}>Something went wrong while fetching the data.</p>
        <Button
          color={'red'}
          content={'Retry now'}
          onClick={onRetryClick}
          data-testid={'retry-btn'}
        />
      </Dimmer>
    </DimmerDimmable>
  )
}
