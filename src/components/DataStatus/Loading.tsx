import { Dimmer, DimmerDimmable, Loader } from 'semantic-ui-react'
import React from 'react'

export const Loading = (): JSX.Element => {
  return (
    <DimmerDimmable as={'div'} blurring={true} className={'Card-loader'}>
      <Dimmer active={true}>
        <Loader data-testid={'loader'}>Loading...</Loader>
      </Dimmer>
    </DimmerDimmable>
  )
}
