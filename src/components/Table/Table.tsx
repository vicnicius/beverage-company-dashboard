import React from 'react'
import {
  Table as LibTable,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from 'semantic-ui-react'
import { Filters } from '../../data/filters'
import { DataContainer } from '../DataContainer/DataContainer'
import { DataStatus, Failed, Loading } from '../DataStatus'
import { TableData } from '../../data/tables'

interface TableProps {
  data: TableData
  status: DataStatus
  onFilterChange: (filters: Filters) => void
  onRetry: () => void
  filters: Filters
  label: string
}

export const Table = ({ data, label, onFilterChange, filters, onRetry, status }: TableProps) => {
  const onRetryClick = () => onRetry()
  const { head, rows } = data
  return (
    <DataContainer
      onFilterChange={onFilterChange}
      label={label}
      filters={filters}
      hidePeriodFilter={true}
    >
      <>
        {status === DataStatus.LOADING ? <Loading /> : null}
        {status === DataStatus.FAILED ? <Failed onRetryClick={onRetryClick} /> : null}
        <LibTable striped={true}>
          <TableHeader>
            <TableRow>
              {head.map((columnName) => (
                <TableHeaderCell key={columnName}>{columnName}</TableHeaderCell>
              ))}
            </TableRow>
          </TableHeader>
          <TableBody>
            {rows.map((row) => (
              // TODO: Pass down a custom row key
              <TableRow key={`${row[0]}${row[row.length - 1]}`}>
                {row.map((item) => (
                  <TableCell key={item}>{item}</TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </LibTable>
      </>
    </DataContainer>
  )
}
