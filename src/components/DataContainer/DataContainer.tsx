import React, { SyntheticEvent, useCallback, useEffect, useRef, useState } from 'react'
import { Filters, PeriodFilter, ValueTypeFilter } from '../../data/filters'
import {
  Button,
  Card,
  CardHeader,
  Checkbox,
  Form,
  FormField,
  Header,
  Popup,
} from 'semantic-ui-react'
import { parsePeriodFilter, parseValueTypeFilter } from '../../data/parser'
import { ChartWrapperProps } from '../Chart'

interface DataContainerProps {
  children: JSX.Element
  filters: Filters
  label: string
  onFilterChange: ChartWrapperProps['onFilterChange']
  hidePeriodFilter: boolean
}

const FilterPanel = ({
  filters,
  closeFilterPanel,
  onFilterChange,
  hidePeriod,
}: {
  filters: Filters
  onFilterChange: (filters: Filters) => void
  closeFilterPanel: () => void
  hidePeriod: boolean
}) => {
  const periodFilterGroup = 'periodFilterGroup'
  const valueTypeFilterGroup = 'valueTypeFilterGroup'
  const onChange = (event: SyntheticEvent<HTMLInputElement>): void => {
    const fieldInput = event.currentTarget.querySelector('input')
    if (!fieldInput) {
      return
    }
    const fieldName = fieldInput.getAttribute('name')
    if (fieldName === periodFilterGroup) {
      const value = parsePeriodFilter(fieldInput.value)
      onFilterChange({ ...filters, period: value })
    }

    if (fieldName === valueTypeFilterGroup) {
      const value = parseValueTypeFilter(fieldInput.value)
      onFilterChange({ ...filters, valueType: value })
    }
    closeFilterPanel()
  }
  return (
    <Form className={'Card-filterPanel'}>
      {!hidePeriod && (
        <>
          <FormField>Filter period by:</FormField>
          <FormField>
            <Checkbox
              radio={true}
              label="Week"
              name={periodFilterGroup}
              value={PeriodFilter.WEEKLY}
              checked={filters.period === PeriodFilter.WEEKLY}
              onChange={onChange}
            />
          </FormField>
          <FormField>
            <Checkbox
              radio={true}
              label="Month"
              name={periodFilterGroup}
              value={PeriodFilter.MONTHLY}
              checked={filters.period === PeriodFilter.MONTHLY}
              onChange={onChange}
            />
          </FormField>
        </>
      )}

      <FormField>Filter revenue by:</FormField>
      <FormField>
        <Checkbox
          radio={true}
          label="Revenue"
          name={valueTypeFilterGroup}
          value={ValueTypeFilter.REVENUE}
          checked={filters.valueType === ValueTypeFilter.REVENUE}
          onChange={onChange}
        />
      </FormField>
      <FormField>
        <Checkbox
          radio={true}
          label="Margin"
          name={valueTypeFilterGroup}
          value={ValueTypeFilter.MARGIN}
          checked={filters.valueType === ValueTypeFilter.MARGIN}
          onChange={onChange}
        />
      </FormField>
    </Form>
  )
}

export const DataContainer = ({
  children,
  filters,
  onFilterChange,
  label,
  hidePeriodFilter,
}: DataContainerProps): JSX.Element => {
  const [filterPanelOpened, setFilterPanelOpened] = useState<boolean>(false)
  const filtersRef = useRef<HTMLDivElement>(null)
  const onFilterToggle = () => {
    setFilterPanelOpened(!filterPanelOpened)
  }
  const filterValueLabel =
    filters.valueType === ValueTypeFilter.REVENUE ? 'Total Revenue' : 'Margin'
  const filterPeriodLabel = filters.period === PeriodFilter.MONTHLY ? ', Monthly' : ', Weekly'
  const closePanel = useCallback(() => setFilterPanelOpened(false), [])
  const closePanelOnClick = useCallback(
    (event: MouseEvent) => {
      if (event.target instanceof HTMLElement && !filtersRef.current?.contains(event.target)) {
        closePanel()
      }
    },
    [filtersRef, closePanel]
  )

  useEffect(() => {
    document.addEventListener('click', closePanelOnClick)
    return () => document.removeEventListener('click', closePanelOnClick)
  }, [closePanelOnClick])

  return (
    <Card centered={true} raised={true} className={'Card'}>
      <CardHeader className={'Card-header'}>
        <Header as={'h3'} textAlign={'right'} className={'Card-headerTitle'}>
          {label}
        </Header>
        <div className={'Card-filters'} ref={filtersRef}>
          <Popup
            content={'Change chart filter'}
            trigger={<Button onClick={onFilterToggle} icon={'filter'} />}
          />
          {filterPanelOpened && (
            <FilterPanel
              onFilterChange={onFilterChange}
              filters={filters}
              hidePeriod={hidePeriodFilter}
              closeFilterPanel={closePanel}
            />
          )}
          <span className={'Card-filtersInfo'}>
            Filtering by <b>{filterValueLabel}</b>
            {!hidePeriodFilter && <b>{filterPeriodLabel}</b>}
          </span>
        </div>
      </CardHeader>
      {children}
    </Card>
  )
}
