import React from 'react'
import { DataPoint } from '../../data/charts'
import {
  Bar,
  BarChart as LibBarChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'

interface BarChartProps {
  dataset: DataPoint[]
  unit: string
}

const formatValue = (value: string) => {
  return `$${Number.parseFloat(value).toFixed(2)}K`
}

export const BarChart = ({ dataset, unit }: BarChartProps): JSX.Element => {
  return (
    <ResponsiveContainer width={'100%'} height={'100%'} className="ChartContainer">
      <LibBarChart data={dataset} margin={{ top: 0, bottom: 0, left: 20, right: 20 }}>
        <CartesianGrid color={'#ccc'} strokeDasharray="5 5" />
        <XAxis dataKey="name" />
        <YAxis unit={unit} />
        <Tooltip formatter={formatValue} />
        <Bar dataKey="value" fill="#2a9d8f" />
      </LibBarChart>
    </ResponsiveContainer>
  )
}
