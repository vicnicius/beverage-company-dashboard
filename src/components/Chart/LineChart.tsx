import React from 'react'
import {
  CartesianGrid,
  Line,
  LineChart as LibLineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'
import { DataPoint } from '../../data/charts'

interface LineChartProps {
  dataset: DataPoint[]
  unit: string
}

const formatValue = (value: string) => {
  return `$${Number.parseFloat(value).toFixed(2)}K`
}

// @Fixme Remove weak week detection and allow consumer of component to pass a format function instead
const formatDate = (value: string) => {
  if (value && value[5] === 'W') {
    return `Week ${value.slice(-2)}`
  }
  return value
}

export const LineChart = ({ dataset, unit }: LineChartProps): JSX.Element => {
  return (
    <ResponsiveContainer width={'100%'} height={'100%'} className="ChartContainer">
      <LibLineChart data={dataset} margin={{ top: 0, bottom: 0, left: 20, right: 20 }}>
        <Line type={'monotone'} dataKey={'value'} stroke={'#264653'} strokeWidth={2} />
        <CartesianGrid color={'#ccc'} strokeDasharray="5 5" />
        <XAxis dataKey="name" tickFormatter={formatDate} />
        <YAxis unit={unit} type={'number'} allowDecimals={false} />
        <Tooltip formatter={formatValue} />
      </LibLineChart>
    </ResponsiveContainer>
  )
}
