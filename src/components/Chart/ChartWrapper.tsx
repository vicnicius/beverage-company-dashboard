import React from 'react'
import { Filters } from '../../data/filters'
import { LineChart } from './LineChart'
import { BarChart } from './BarChart'
import { DataPoint } from '../../data/charts'
import { DataContainer } from '../DataContainer/DataContainer'
import { DataStatus, Failed, Loading } from '../DataStatus'

export enum ChartType {
  BAR = 'Bar',
  LINE = 'Line',
}

export interface ChartWrapperProps {
  chartType: ChartType
  dataset: DataPoint[]
  filters: Filters
  label: string
  unit: string
  onRetry: () => void
  onFilterChange: (filters: Filters) => void
  status: DataStatus
  hidePeriodFilter?: boolean
}

export const ChartWrapper = (props: ChartWrapperProps): JSX.Element => {
  const { status, onRetry, dataset, chartType, onFilterChange, label, unit, filters } = props
  const onRetryClick = () => onRetry()

  if (chartType === ChartType.LINE) {
    return (
      <DataContainer
        onFilterChange={onFilterChange}
        label={label}
        filters={filters}
        hidePeriodFilter={!!props.hidePeriodFilter}
      >
        <>
          {status === DataStatus.LOADING ? <Loading /> : null}
          {status === DataStatus.FAILED ? <Failed onRetryClick={onRetryClick} /> : null}
          <LineChart dataset={dataset} unit={unit} />
        </>
      </DataContainer>
    )
  }

  return (
    <DataContainer
      onFilterChange={onFilterChange}
      label={label}
      filters={filters}
      hidePeriodFilter={!!props.hidePeriodFilter}
    >
      <>
        {status === DataStatus.LOADING ? <Loading /> : null}
        {status === DataStatus.FAILED ? <Failed onRetryClick={onRetryClick} /> : null}
        <BarChart dataset={dataset} unit={unit} />
      </>
    </DataContainer>
  )
}
