import React from 'react'
import { cleanup, render, screen } from '@testing-library/react'
import { ChartType, ChartWrapper, ChartWrapperProps } from './ChartWrapper'
import { PeriodFilter, ValueTypeFilter } from '../../data/filters'
import { LineChart as LineChartMock } from './LineChart'
import { BarChart as BarChartMock } from './BarChart'
import userEvent from '@testing-library/user-event'
import { DataStatus } from '../DataStatus'

interface OverrideArgs {
  chartType?: ChartWrapperProps['chartType']
  dataset?: ChartWrapperProps['dataset']
  filters?: ChartWrapperProps['filters']
  status?: ChartWrapperProps['status']
  unit?: ChartWrapperProps['unit']
  onFilterChange?: ChartWrapperProps['onFilterChange']
  onRetry?: ChartWrapperProps['onRetry']
}

jest.mock('./LineChart', () => ({
  LineChart: jest.fn(() => <div />),
}))

jest.mock('./BarChart', () => ({
  BarChart: jest.fn(() => <div />),
}))

const getProps: (override?: OverrideArgs) => ChartWrapperProps = (override = {}) => {
  return {
    chartType: ChartType.LINE,
    dataset: [],
    filters: {
      valueType: ValueTypeFilter.REVENUE,
      period: PeriodFilter.MONTHLY,
    },
    unit: 'test',
    label: 'test',
    status: DataStatus.INITIAL,
    onFilterChange: () => {
      return
    },
    onRetry: () => {
      return
    },
    ...override,
  }
}

describe('ChartWrapper component', () => {
  beforeEach(jest.clearAllMocks)
  afterAll(cleanup)
  it('should show a loading spinner when data is still being loaded', () => {
    const props = getProps({ status: DataStatus.LOADING })
    render(<ChartWrapper {...props} />)
    const loader = screen.getByTestId('loader')
    expect(loader).toBeInTheDocument()
  })
  it('should show an error message when it fails to load the data', () => {
    const props = getProps({ status: DataStatus.FAILED })
    render(<ChartWrapper {...props} />)
    const loader = screen.getByTestId('error-message')
    expect(loader).toBeInTheDocument()
  })
  it('should allow retrying to fetch the data when it fails to load the data', () => {
    const onRetry = jest.fn()
    const props = getProps({ status: DataStatus.FAILED, onRetry })
    render(<ChartWrapper {...props} />)
    const retryBtn = screen.getByTestId('retry-btn')
    userEvent.click(retryBtn)
    expect(onRetry).toHaveBeenCalled()
  })
  it('should show a line chart with the given dataset when its data is loaded and the selected type is line', () => {
    const mockDataset = [{ name: 'week 1', value: 10 }]
    const mockUnit = 'unit'
    const props = getProps({
      chartType: ChartType.LINE,
      dataset: mockDataset,
      status: DataStatus.LOADED,
      unit: mockUnit,
    })
    render(<ChartWrapper {...props} />)
    expect(LineChartMock).toHaveBeenCalledWith({ dataset: mockDataset, unit: mockUnit }, {})
  })
  it('should show a bar chart with the given dataset when its data is loaded and the selected type is line', () => {
    const mockDataset = [{ name: 'week 1', value: 10 }]
    const mockUnit = 'unit'
    const props = getProps({
      chartType: ChartType.BAR,
      dataset: mockDataset,
      status: DataStatus.LOADED,
      unit: mockUnit,
    })
    render(<ChartWrapper {...props} />)
    expect(BarChartMock).toHaveBeenCalledWith({ dataset: mockDataset, unit: mockUnit }, {})
  })
  it.todo('should allow setting a filter')
})
