import React from 'react'
import { Grid, GridColumn, GridRow, Segment } from 'semantic-ui-react'

import 'semantic-ui-less/semantic.less'
import logo from './logo.svg'
import './App.less'
import { CumulativeInvoicesChart } from './useCase/CumulativeInvoicesChart'
import { TotalRevenuePerProductChart } from './useCase/TotalRevenuePerProductChart'
import { LatestInvoicesByDateTable } from './useCase/LatestInvoicesByDateTable'
import { BestCustomersTable } from './useCase/BestCustomersTable'

function App() {
  return (
    <div className="App">
      <Segment as="header" className="App-header" basic={true}>
        <span>
          <img src={logo} className="App-logo" alt="logo" />
        </span>
        <span className="App-companyName">The Beverage Company</span>
      </Segment>
      <Segment as="main" data-testid={'main'} className="Main">
        <Grid divided={'vertically'} stackable={true}>
          <GridRow columns={2}>
            <GridColumn>
              <CumulativeInvoicesChart />
            </GridColumn>
            <GridColumn>
              <TotalRevenuePerProductChart />
            </GridColumn>
          </GridRow>
          <GridRow columns={2}>
            <GridColumn>
              <LatestInvoicesByDateTable />
            </GridColumn>
            <GridColumn>
              <BestCustomersTable />
            </GridColumn>
          </GridRow>
        </Grid>
      </Segment>
    </div>
  )
}

export default App
