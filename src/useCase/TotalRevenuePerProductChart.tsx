import React, { useEffect, useMemo, useState } from 'react'
import { CategoryRevenue, categoryRevenueToDataset, getRevenuePerCategory } from '../data/revenues'
import { ChartType, ChartWrapper } from '../components/Chart'
import { DataPoint } from '../data/charts'
import { defaultFilters, Filters } from '../data/filters'
import { DataStatus } from '../components/DataStatus'

export const TotalRevenuePerProductChart = () => {
  const [revenue, setRevenue] = useState<CategoryRevenue[]>([])
  const [currentDataStatus, setDataStatus] = useState<DataStatus>(DataStatus.INITIAL)
  const [filters, setFilters] = useState<Filters>(defaultFilters)
  const dataset = useMemo<DataPoint[]>(() => {
    return categoryRevenueToDataset(revenue, filters.valueType)
  }, [revenue, filters])
  const onFilterChange = (updatedFilters: Filters) => {
    setFilters(updatedFilters)
  }

  // @TODO: Implement refetch
  const onRetry = () => {
    // tslint:disable-next-line:no-console
    console.log('Retry fetching...')
  }

  useEffect(() => {
    setDataStatus(DataStatus.LOADING)
    getRevenuePerCategory()
      .then((newRevenue) => {
        setDataStatus(DataStatus.LOADED)
        setRevenue(newRevenue)
      })
      .catch((error) => {
        // tslint:disable-next-line:no-console
        console.error(error)
        setDataStatus(DataStatus.FAILED)
      })
  }, [filters])
  return (
    <ChartWrapper
      dataset={dataset}
      status={currentDataStatus}
      chartType={ChartType.BAR}
      filters={filters}
      label={'Total Revenue Per Category'}
      unit={'K $'}
      onFilterChange={onFilterChange}
      onRetry={onRetry}
      hidePeriodFilter={true}
    />
  )
}
