import React, { useEffect, useMemo, useState } from 'react'
import { cumulativeRevenueToDataset, getRevenue, Revenue } from '../data/revenues'
import { ChartType, ChartWrapper } from '../components/Chart'
import { DataPoint } from '../data/charts'
import { defaultFilters, Filters } from '../data/filters'
import { DataStatus } from '../components/DataStatus'

export const CumulativeInvoicesChart = () => {
  const [revenue, setRevenue] = useState<Revenue[]>([])
  const [currentDataStatus, setDataStatus] = useState<DataStatus>(DataStatus.INITIAL)
  const [filters, setFilters] = useState<Filters>(defaultFilters)
  const dataset = useMemo<DataPoint[]>(() => {
    return cumulativeRevenueToDataset(revenue, filters)
  }, [revenue, filters])
  const onFilterChange = (updatedFilters: Filters) => {
    setFilters(updatedFilters)
  }

  // @TODO: Implement refetch
  const onRetry = () => {
    // tslint:disable-next-line:no-console
    console.log('Retry fetching...')
  }

  useEffect(() => {
    setDataStatus(DataStatus.LOADING)
    getRevenue(filters)
      .then((newRevenue) => {
        setDataStatus(DataStatus.LOADED)
        setRevenue(newRevenue)
      })
      .catch((error) => {
        // tslint:disable-next-line:no-console
        console.error(error)
        setDataStatus(DataStatus.FAILED)
      })
  }, [filters])
  return (
    <ChartWrapper
      dataset={dataset}
      status={currentDataStatus}
      chartType={ChartType.LINE}
      filters={filters}
      label={'Cumulative Invoices Revenue'}
      unit={'K $'}
      onFilterChange={onFilterChange}
      onRetry={onRetry}
    />
  )
}
