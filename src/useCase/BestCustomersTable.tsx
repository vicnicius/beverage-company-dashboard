import React, { useEffect, useMemo, useState } from 'react'
import { defaultFilters, Filters, ValueTypeFilter } from '../data/filters'
import { DataStatus } from '../components/DataStatus'
import { getBestCustomersByValueType, Invoice, latestInvoicesToTableRows } from '../data/invoices'
import { TableData } from '../data/tables'
import { Table } from '../components/Table/Table'

export const BestCustomersTable = () => {
  const [invoices, setInvoices] = useState<Invoice[]>([])
  const [currentDataStatus, setDataStatus] = useState<DataStatus>(DataStatus.INITIAL)
  const [filters, setFilters] = useState<Filters>(defaultFilters)
  const dataset = useMemo<TableData>(() => {
    const head = ['Customer Name', 'Region', '# of Invoices']
    const baseRows: (keyof Invoice)[] = ['customerName', 'region', 'numberOfInvoices']

    if (filters.valueType === ValueTypeFilter.MARGIN) {
      head.push('Total Margin ($)')
      return {
        head,
        rows: latestInvoicesToTableRows(invoices, [...baseRows, 'totalMargin']),
      }
    }

    head.push('Total ($)')
    return {
      head,
      rows: latestInvoicesToTableRows(invoices, [...baseRows, 'totalInvoiceRevenue']),
    }
  }, [invoices, filters])
  const onFilterChange = (updatedFilters: Filters) => {
    setFilters(updatedFilters)
  }

  // @TODO: Implement refetch
  const onRetry = () => {
    // tslint:disable-next-line:no-console
    console.log('Retry fetching...')
  }

  useEffect(() => {
    setDataStatus(DataStatus.LOADING)
    getBestCustomersByValueType(filters.valueType)
      .then((newInvoices) => {
        setDataStatus(DataStatus.LOADED)
        setInvoices(newInvoices)
      })
      .catch((error) => {
        // tslint:disable-next-line:no-console
        console.error(error)
        setDataStatus(DataStatus.FAILED)
      })
  }, [filters])
  return (
    <Table
      data={dataset}
      status={currentDataStatus}
      filters={filters}
      onFilterChange={onFilterChange}
      onRetry={onRetry}
      label={'Top 15 Best Customers'}
    />
  )
}
