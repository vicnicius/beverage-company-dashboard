import React, { useEffect, useMemo, useState } from 'react'
import { defaultFilters, Filters, ValueTypeFilter } from '../data/filters'
import { DataStatus } from '../components/DataStatus'
import { getLatestInvoicesByDate, Invoice, latestInvoicesToTableRows } from '../data/invoices'
import { TableData } from '../data/tables'
import { Table } from '../components/Table/Table'

export const LatestInvoicesByDateTable = () => {
  const [invoices, setInvoices] = useState<Invoice[]>([])
  const [currentDataStatus, setDataStatus] = useState<DataStatus>(DataStatus.INITIAL)
  const [filters, setFilters] = useState<Filters>(defaultFilters)
  const dataset = useMemo<TableData>(() => {
    const head = ['Id', 'Date', 'Customer Name', 'Region']

    if (filters.valueType === ValueTypeFilter.MARGIN) {
      head.push('Total Margin ($)')
      return {
        head,
        rows: latestInvoicesToTableRows(invoices, [
          'id',
          'date',
          'customerName',
          'region',
          'totalMargin',
        ]),
      }
    }

    head.push('Total ($)')
    return {
      head,
      rows: latestInvoicesToTableRows(invoices, [
        'id',
        'date',
        'customerName',
        'region',
        'totalInvoiceRevenue',
      ]),
    }
  }, [invoices, filters])
  const onFilterChange = (updatedFilters: Filters) => {
    setFilters(updatedFilters)
  }

  // @TODO: Implement refetch
  const onRetry = () => {
    // tslint:disable-next-line:no-console
    console.log('Retry fetching...')
  }

  useEffect(() => {
    setDataStatus(DataStatus.LOADING)
    getLatestInvoicesByDate(15)
      .then((newInvoices) => {
        setDataStatus(DataStatus.LOADED)
        // Getting the first 15 only
        setInvoices(newInvoices)
      })
      .catch((error) => {
        // tslint:disable-next-line:no-console
        console.error(error)
        setDataStatus(DataStatus.FAILED)
      })
  }, [filters])
  return (
    <Table
      data={dataset}
      status={currentDataStatus}
      filters={filters}
      onFilterChange={onFilterChange}
      onRetry={onRetry}
      label={'Latest Invoices By Date'}
    />
  )
}
