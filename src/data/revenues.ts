import { parseDate, parseNumber, parseString } from './parser'
import { DataPoint } from './charts'
import { Filters, PeriodFilter, ValueTypeFilter } from './filters'

interface BaseRevenue {
  startDate: Date
  endDate: Date
  invoicesCount: number
  totalMargin: number
  totalRevenue: number
}

export interface CategoryRevenue {
  categoryName: string
  totalRevenue: number
  totalMargin: number
}

interface WeeklyRevenue extends BaseRevenue {
  week: string
}

interface MonthlyRevenue extends BaseRevenue {
  month: string
}

export enum RevenueType {
  WEEKLY = 'Weekly',
  MONTHLY = 'Monthly',
}

export type Revenue = WeeklyRevenue | MonthlyRevenue

const getEndpoint = (filter: PeriodFilter): string => {
  const period = filter === PeriodFilter.WEEKLY ? 'weekly' : 'monthly'
  // @TODO Extract api host and path and move it to config + env vars
  return `http://localhost:3001/api/revenues/${period}`
}

const isWeeklyRevenue = (revenue: Revenue): revenue is WeeklyRevenue => {
  return (revenue as WeeklyRevenue).week !== undefined
}

const isMonthlyRevenue = (revenue: Revenue): revenue is MonthlyRevenue => {
  return (revenue as MonthlyRevenue).month !== undefined
}

const parseResponse = (response: any): Revenue[] => {
  if (!Array.isArray(response)) {
    throw new Error(
      `Unable to parse revenue response. Expected array but got ${typeof response} instead`
    )
  }
  return response.map((item) => {
    const baseRevenue = {
      startDate: parseDate(item.start_date),
      endDate: parseDate(item.end_date),
      invoicesCount: parseNumber(item.invoices_count),
      totalMargin: parseNumber(item.total_margin),
      totalRevenue: parseNumber(item.total_revenue),
    }

    if (isWeeklyRevenue(item)) {
      return {
        ...baseRevenue,
        week: parseString(item.week),
      }
    }

    return {
      ...baseRevenue,
      month: parseString(item.month),
    }
  })
}

const parseCategoryRevenueResponse = (response: any): CategoryRevenue[] => {
  if (!Array.isArray(response)) {
    throw new Error(
      `Unable to parse category revenue response. Expected array but got ${typeof response} instead`
    )
  }
  return response.map((item) => ({
    categoryName: parseString(item.category_name),
    totalRevenue: parseNumber(item.total_revenue),
    totalMargin: parseNumber(item.total_margin),
  }))
}

const sortWeeklyRevenue = (a: Revenue, b: Revenue): number => {
  if (!isWeeklyRevenue(a) || !isWeeklyRevenue(b)) {
    throw new Error('Error while trying to sort weekly revenue.')
  }
  if (a.week > b.week) {
    return 1
  }
  if (a.week < b.week) {
    return -1
  }
  return 0
}

const sortMonthlyRevenue = (a: Revenue, b: Revenue): number => {
  if (!isMonthlyRevenue(a) || !isMonthlyRevenue(b)) {
    throw new Error('Error while trying to sort monthly revenue.')
  }
  if (a.month > b.month) {
    return 1
  }
  if (a.month < b.month) {
    return -1
  }
  return 0
}

const sortCategoryRevenue = (a: CategoryRevenue, b: CategoryRevenue): number => {
  if (a.categoryName > b.categoryName) {
    return 1
  }
  if (a.categoryName < b.categoryName) {
    return -1
  }
  return 0
}

export const getRevenue = async (filters: Filters): Promise<Revenue[]> => {
  const revenueEndpoint = getEndpoint(filters.period)
  const rawResponse: Response = await fetch(revenueEndpoint, {
    method: 'GET',
  })
  const response = await rawResponse.json()

  return parseResponse(response).sort((a, b) => {
    if (isWeeklyRevenue(a) && isWeeklyRevenue(b)) {
      return sortWeeklyRevenue(a, b)
    }
    if (isMonthlyRevenue(a) && isMonthlyRevenue(b)) {
      return sortMonthlyRevenue(a, b)
    }
    throw new Error('Error trying to sort revenue.')
  })
}

export const getRevenuePerCategory = async (): Promise<CategoryRevenue[]> => {
  const endpoint = 'http://localhost:3001/api/categories/revenues'
  const rawResponse: Response = await fetch(endpoint, { method: 'GET' })
  const response = await rawResponse.json()
  return parseCategoryRevenueResponse(response).sort(sortCategoryRevenue)
}

export const cumulativeRevenueToDataset = (revenues: Revenue[], filters: Filters): DataPoint[] => {
  let cumulativeValue = 0
  return revenues.map((revenue) => {
    const toAdd =
      filters.valueType === ValueTypeFilter.REVENUE ? revenue.totalRevenue : revenue.totalMargin
    // @TODO: Check floating point sum precision - consider using https://github.com/MikeMcl/big.js
    cumulativeValue = cumulativeValue + toAdd
    const name = isWeeklyRevenue(revenue) ? revenue.week : revenue.month
    return {
      name,
      value: cumulativeValue / 1000,
    }
  })
}

// Returns revenue in 1000s
export const categoryRevenueToDataset = (
  categoryRevenues: CategoryRevenue[],
  filter: ValueTypeFilter
): DataPoint[] => {
  return categoryRevenues.map((categoryRevenue) => {
    return {
      name: categoryRevenue.categoryName,
      value:
        filter === ValueTypeFilter.REVENUE
          ? categoryRevenue.totalRevenue / 1000
          : categoryRevenue.totalMargin / 1000,
    }
  })
}
