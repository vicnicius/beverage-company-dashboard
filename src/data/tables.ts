export interface TableData {
  head: string[]
  rows: string[][]
}
