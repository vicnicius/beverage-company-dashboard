import { parseDate, parseNumber, parseString } from './parser'
import { TableData } from './tables'
import { ValueTypeFilter } from './filters'

export interface Invoice {
  id: number
  customerId: number
  customerName: string
  date: Date
  totalInvoiceRevenue: number
  totalMargin: number
  region: string
  numberOfInvoices: number
}

const parseResponse = (response: any): Invoice[] => {
  if (!Array.isArray(response)) {
    throw new Error(
      `Unable to parse invoices response. Expected array but got ${typeof response} instead.`
    )
  }
  return response.map((item) => {
    return {
      id: parseNumber(item.id),
      customerId: parseNumber(item.customer_id),
      customerName: parseString(item.customer_name),
      date: parseDate(item.date),
      totalInvoiceRevenue: parseNumber(item.total_invoice),
      totalMargin: parseNumber(item.total_margin),
      region: parseString(item.region),
      numberOfInvoices: item.invoice_lines.length,
    }
  })
}

export const getLatestInvoicesByDate = async (numberOfInvoice: number = 10): Promise<Invoice[]> => {
  const endpoint = `http://localhost:3001/api/invoices?_sort=date&_order=desc&_limit=${numberOfInvoice}`
  const rawResponse = await fetch(endpoint, { method: 'GET' })
  const response = await rawResponse.json()
  return parseResponse(response)
}

export const getBestCustomersByValueType = async (
  valueType: ValueTypeFilter
): Promise<Invoice[]> => {
  const endpoint = `http://localhost:3001/api/invoices?_sort=${
    valueType === ValueTypeFilter.MARGIN ? 'total_margin' : 'total_invoice'
  }&_order=desc&_limit=15`
  const rawResponse = await fetch(endpoint, { method: 'GET' })
  const response = await rawResponse.json()
  return parseResponse(response)
}

export const latestInvoicesToTableRows = (
  invoices: Invoice[],
  columnKeys: (keyof Invoice)[]
): TableData['rows'] => {
  return invoices.map((invoice) => {
    const row: string[] = []
    columnKeys.forEach((columnKey) => {
      if (columnKey === 'date') {
        return row.push(invoice[columnKey].toLocaleDateString())
      }
      if (columnKey === 'totalInvoiceRevenue' || columnKey === 'totalMargin') {
        return row.push(invoice[columnKey].toFixed(2))
      }
      row.push(invoice[columnKey].toString())
    })
    return row
  })
}
