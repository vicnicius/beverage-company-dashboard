import { PeriodFilter, ValueTypeFilter } from './filters'

export const parseDate = (value: any): Date => {
  return new Date(value)
}

export const parseNumber = (value: any): number => {
  if (typeof value !== 'number') {
    throw new Error(`Failed to parse number. Expected number but got ${typeof value} instead.`)
  }
  return value
}

export const parseString = (value: any): string => {
  if (typeof value !== 'string') {
    throw new Error(`Failed to parse string. Expected string but got ${typeof value} instead.`)
  }
  return value
}

export const parsePeriodFilter = (value: string): PeriodFilter => {
  if (value === PeriodFilter.WEEKLY) {
    return PeriodFilter.WEEKLY
  }
  if (value === PeriodFilter.MONTHLY) {
    return PeriodFilter.MONTHLY
  }
  throw new Error(
    `Invalid period filter received. Expected one of ${PeriodFilter.WEEKLY} | ${PeriodFilter.MONTHLY}, but got ${value} instead.`
  )
}

export const parseValueTypeFilter = (value: string): ValueTypeFilter => {
  if (value === ValueTypeFilter.REVENUE) {
    return ValueTypeFilter.REVENUE
  }
  if (value === ValueTypeFilter.MARGIN) {
    return ValueTypeFilter.MARGIN
  }
  throw new Error(
    `Invalid value type filter received. Expected one of ${ValueTypeFilter.REVENUE} | ${ValueTypeFilter.MARGIN}, but got ${value} instead.`
  )
}
