export enum PeriodFilter {
  WEEKLY = 'Weekly',
  MONTHLY = 'Monthly',
}

export enum ValueTypeFilter {
  REVENUE = 'Revenue',
  MARGIN = 'Margin',
}

export interface Filters {
  valueType: ValueTypeFilter
  period: PeriodFilter
}

export const defaultFilters = {
  valueType: ValueTypeFilter.REVENUE,
  period: PeriodFilter.WEEKLY,
}
