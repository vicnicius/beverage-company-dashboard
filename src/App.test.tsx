import React from 'react'
import { render, screen } from '@testing-library/react'
import App from './App'

test('renders base app structure', () => {
  render(<App />)
  const logoElement = screen.getByText('The Beverage Company')
  const mainElement = screen.getByTestId('main')
  expect(logoElement).toBeInTheDocument()
  expect(mainElement).toBeInTheDocument()
})
